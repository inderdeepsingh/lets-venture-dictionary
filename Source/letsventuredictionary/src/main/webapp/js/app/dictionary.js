/**
 * <pre>
 * API Type: Angular Module,
 * Description :
 * This is an angular Module used to bootstrap the application by angular. 
 * It has associated controller and service which deals with data management on the views
 * </pre>
 */
(function() {
	/* Start:Module Configuration */
	var moduleName = "dictionaryModule";
	// Module Dependencies
	var injectedDependencies = ['ui.router' ];
	/* End Configuration */

	/* Config Function */
	var config = function($stateProvider, $urlRouterProvider) {

		/*
		 * Define different routes and states here
		 */
		$stateProvider.state('bookmark', {
			url : '/bookmark/:bookmarkId',
			templateUrl : 'bookmark.html',
			controller : 'BookmarkController'
		}).state('search', {
			url : '/search',
			templateUrl : 'search.html',		
		});
		/*
		 * Config function injected dependencies
		 */
		config.$inject = [ '$stateProvider', '$urlRouterProvider' ];

		$urlRouterProvider.otherwise('search');
	};

	// Register Module
	angular.module(moduleName, injectedDependencies).config(config);
}());
