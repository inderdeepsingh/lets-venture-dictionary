/**
 * <pre>
 * API Type: Angular Controller,
 * Module: dictionaryModule,
 * Description :
 * This is API used as controller for controlling the index.html views.
 * </pre>
 */
(function() {
	/* Start: Controller Configuration */
	var moduleName = "dictionaryModule";
	var module = angular.module(moduleName);
	var controllerName = "DictionaryController";
	var injectedDependencies = [ '$scope', '$timeout', '$rootScope', 'dictionaryService', '$location', '$window' ];
	/* End: Controller Configuration */

	/* Start:Controller Definition */
	var controller = function($scope, $timeout, $rootScope, dictionaryService, $location, $window) {
		/* To create pagination */
		var alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");

		/* Update Search results in scope */
		var updateSearchResults = function(words) {
			$scope.words = words;
			$scope.searchRunning = false;
			getApiHits();
		};

		/* Update Bookmarks in scope */
		var updateBookmarks = function(bookmarks) {
			$scope.bookmarks = bookmarks;
		};

		/* Update Bookmarks Count in scope */
		var updateBookmarksCount = function(count) {
			$scope.bookmarksCount = count;
		};

		/* Update API Hits in scope */
		var updateApiHits = function(count) {
			$scope.apiHits = count;
		};

		/* Log the Server Error to console */
		var showError = function(error) {
			console.log(error);
			$scope.error = error;
		};

		/** Search the words from server according to alphabet selected */
		var searchWords = function(alphabet) {
			$scope.words = [];
			$scope.searchRunning = true;
			if (alphabet) {
				$scope.searchText = alphabet;
			}
			dictionaryService.searchWords($scope.searchText, updateSearchResults, showError);
		};

		/** Play the audio of the word */
		var playAudio = function(url) {
			var audio = new Audio(url);
			audio.play();
		};

		/** Clear Bookmarks */
		var clearBookmarks = function() {
			var successCallback=function(){
				getBookmarksCount();
				getBookmarks();
			}
			dictionaryService.clearBookmarks(successCallback, showError);
		};

		/*
		 * 
		 * Add a bookmark to list- Upon doing this refresh Bookmarks count and
		 * get API Hits again
		 */
		var addBookmarkToList = function(bookmark) {
			if ($scope.bookmarks) {
				console.log($scope.bookmarks);
				$scope.bookmarks.push(bookmark);
				$scope.bookmarksCount++;
				getApiHits();
			}
		};

		/*
		 * Success callback on saving Bookmark to dictionaryService
		 */
		var saveBookmarkSuccessCallback = function(bookmark) {
			addBookmarkToList(bookmark);
		};

		/**
		 * Save Bookmark to dictionaryService
		 */
		var saveBookmark = function(word) {
			var successCallback=function(bookmark){
				word.bookmarked=true;
				saveBookmarkSuccessCallback(bookmark)
			}
			dictionaryService.saveBookmark(word, successCallback, showError);
		};

		/* Get API Hits from dictionaryService */
		var getApiHits = function() {
			dictionaryService.getApiHits(updateApiHits, showError);
		};

		/* Get Bookmark Count from dictionaryService */
		var getBookmarksCount = function() {
			dictionaryService.getBookmarksCount(updateBookmarksCount, showError);
		};

		/* Get Bookmarks from dictionaryService */
		var getBookmarks = function() {
			dictionaryService.getBookmarks(updateBookmarks, showError);
		};

		/**
		 * Searching the word in the words set in scope using startsWith
		 */
		var startsWith = function(actual) {
			if (actual) {
				var lowerStr = (actual.word + "").toLowerCase();
				return lowerStr.indexOf($scope.searchData.text.toLowerCase()) != -1;
			} else {
				return true;
			}
		};

		/*
		 * This code ensures that angular template is shown only after it is
		 * rendered
		 */
		$timeout(function() {
			$scope.applicationLoaded = true;
		}, 500);

		getApiHits();
		getBookmarksCount();
		getBookmarks();
		$scope.searchWords = searchWords;
		$scope.saveBookmark = saveBookmark;
		$scope.clearBookmarks = clearBookmarks;
		$scope.alphabets = alphabets;
		$scope.playAudio = playAudio;
		$scope.startsWith = startsWith;
		/*
		 * Predefined the searchData since it is to be referred in ng-include
		 * inherited scope
		 */
		$scope.searchData = {
			text : ''
		};
	};
	/* End:Controller Definition */

	/* Register the controller */
	controller.$inject = injectedDependencies;
	module.controller(controllerName, controller);
}());
