/**
 * <pre>
 * API Type:Angular Service,
 * Module: dictionaryModule,
 * Description :
 * This is API used for calling Dictionary Rest API's on server .
 * </pre>
 */
(function() {
	/* Start:Service Configuration */
	var moduleName = "dictionaryModule";
	var module = angular.module(moduleName);
	var serviceName = "dictionaryService";
	var injectedDependencies = [ '$http' ];
	/* End: Configuration */

	/* Start:Service Definition */
	var service = function($http) {

		/**
		 * Save a Bookmark to server
		 * 
		 * @param wordInfo- Bookmark object
		 * @param successCallback- Success callback to be executed
		 * @param failCallback- Fail callback to be executed
		 */
		var saveBookmark = function(wordInfo, successCallback, failCallback) {
			var success = function(response) {
				if (successCallback) {
					successCallback(response.data);
				}
			};
			var fail = function(response) {
				if (failCallback) {
					failCallback(response.data, response);
				}
			}
			$http.post("dictionary/bookmark", wordInfo).then(success, fail);
		};

		/**
		 * Search Words starting with input text
		 * 
		 * @param input text
		 * @param successCallback- Success callback to be executed
		 * @param failCallback- Fail callback to be executed
		 */
		var searchWords = function(input, successCallback, failCallback) {
			var success = function(response) {
				if (successCallback) {
					successCallback(response.data, response);
				}
			};
			var fail = function(response) {

				if (failCallback) {
					failCallback(response.data, response);
				}
			}
			$http.get("dictionary/words/" + input).then(success, fail);
		};

		/**
		 * Get Bookmarked Words from server
		 * 
		 * @param successCallback- Success callback to be executed
		 * @param failCallback- Fail callback to be executed
		 */
		var getBookmarks = function(successCallback, failCallback) {
			var success = function(response) {
				if (successCallback) {
					successCallback(response.data, response);
				}
			};
			var fail = function(response) {

				if (failCallback) {
					failCallback(response.data, response);
				}
			}
			$http.get("dictionary/bookmarks/").then(success, fail);
		};

		/**
		 * Returns API Hits to the Rest Service
		 * 
		 * @param input text
		 * @param successCallback- Success callback to be executed
		 * @param failCallback- Fail callback to be executed
		 */
		var getApiHits = function(successCallback, failCallback) {
			var success = function(response) {
				if (successCallback) {
					successCallback(response.data, response);
				}
			};
			var fail = function(response) {

				if (failCallback) {
					failCallback(response.data, response);
				}
			}
			$http.get("dictionary/apihits/").then(success, fail);
		};

		/**
		 * Returns Bookmark count
		 * 
		 * @param successCallback- Success callback to be executed
		 * @param failCallback- Fail callback to be executed
		 */
		var getBookmarksCount = function(successCallback, failCallback) {
			var success = function(response) {
				if (successCallback) {
					successCallback(response.data, response);
				}
			};
			var fail = function(response) {

				if (failCallback) {
					failCallback(response.data, response);
				}
			}
			$http.get("dictionary/bookmarks/count").then(success, fail);
		};
		
		
		
		/**
		 * Clear Bookmarks
		 * 
		 * @param successCallback- Success callback to be executed
		 * @param failCallback- Fail callback to be executed
		 */
		var clearBookmarks = function(successCallback, failCallback) {
			var success = function(response) {
				if (successCallback) {
					successCallback(response.data, response);
				}
			};
			var fail = function(response) {

				if (failCallback) {
					failCallback(response.data, response);
				}
			}
			$http.get("dictionary/bookmarks/clear").then(success, fail);
		};
		return {
			'saveBookmark' : saveBookmark,
			'searchWords' : searchWords,
			'getBookmarks' : getBookmarks,
			'getApiHits' : getApiHits,
			'getBookmarksCount' : getBookmarksCount,
			'clearBookmarks' : clearBookmarks
		};
	};
	/* End:Service Declaration */

	/* Start:Service Registration */
	service.$inject = injectedDependencies;
	module.factory(serviceName, service);
	/* End:Service Registration */
}());