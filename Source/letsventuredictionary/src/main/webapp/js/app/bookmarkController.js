/**
 * <pre>
 * API Type: Angular Controller,
 * Module: dictionaryModule,
 * Description :
 * This is API used as controller for controlling the bookmark.html view.
 * </pre>
 */
(function() {
	/* Start: Controller Configuration */
	var moduleName = "dictionaryModule";
	var module = angular.module(moduleName);
	var controllerName = "BookmarkController";
	var injectedDependencies = [ '$scope', '$timeout', '$rootScope', '$stateParams', '$filter' ];
	/* End: Controller Configuration */

	/* Start:Controller Definition */
	var controller = function($scope, $timeout, $rootScope, $stateParams, $filter) {
		/*Get Bookmark upon loading of the Bookmark route*/
		var getBookmark = function() {
			var scopeBookmarkId = $stateParams.bookmarkId;

			$scope.bookmark = $filter('filter')($scope.bookmarks, {
				bookmarkId : scopeBookmarkId
			})[0];

		};
		getBookmark();
	};
	/* End:Controller Definition */

	/* Register the controller */
	controller.$inject = injectedDependencies;
	module.controller(controllerName, controller);
}());
