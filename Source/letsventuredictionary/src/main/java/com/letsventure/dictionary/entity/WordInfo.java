package com.letsventure.dictionary.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Inderdeep Singh JPA Embeddable class to represent a Word
 *
 */
@Embeddable
@JsonIgnoreProperties(ignoreUnknown = true)
public class WordInfo {

	@Column(name = "WORD", nullable = false, length = 500)
	@NotNull
	private String word;

	@Column(name = "WORD_DESC", length = 5000)
	private String description;

	@Column(name = "WORD_AUDIO_URL", length = 500)
	private String audio_url;
	
	
	private transient boolean bookmarked=true;
	
	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAudio_url() {
		return audio_url;
	}

	public void setAudio_url(String audio_url) {
		this.audio_url = audio_url;
	}

	public boolean isBookmarked() {
		return bookmarked;
	}

	public void setBookmarked(boolean bookmarked) {
		this.bookmarked = bookmarked;
	}

}
