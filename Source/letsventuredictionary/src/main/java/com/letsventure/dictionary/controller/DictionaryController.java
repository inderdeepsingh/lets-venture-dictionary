package com.letsventure.dictionary.controller;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.letsventure.dictionary.entity.Bookmark;
import com.letsventure.dictionary.entity.WordInfo;
import com.letsventure.dictionary.service.DictionaryService;
import com.letsventure.dictionary.service.RestService;

/**
 * @author Inderdeep Singh Controller class to expose the Dictionary Rest API
 *
 */
@RestController
@RequestMapping("/dictionary")
public class DictionaryController {

	@Autowired
	private DictionaryService dictionaryService;

	@Autowired
	private RestService restService;

	/**
	 * Search words starting with
	 * 
	 * @param startingWith
	 * @return List of words
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/words/{startingWith}", produces = "application/json")
	@ResponseBody
	public List<WordInfo> searchWords(@PathVariable String startingWith) throws Exception {
		return dictionaryService.getWords(startingWith);
	}

	/**
	 * Save a bookmark to Database
	 * 
	 * @param info
	 * @return saved bookmark
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/bookmark", produces = "application/json", consumes = "application/json")
	@ResponseBody
	public Bookmark saveBookmark(@RequestBody WordInfo info) throws Exception {
		if (info != null) {
			Bookmark bookmark = new Bookmark();
			bookmark.setWordInfo(info);
			return this.dictionaryService.saveBookmark(bookmark);
		}
		return null;
	}

	/**
	 * @return Bookmark List
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/bookmarks", produces = "application/json")
	@ResponseBody
	public List<Bookmark> getBookmarks() throws Exception {
		return this.dictionaryService.getBookmarks();
	}

	/**
	 * @return count of API Hits
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/apihits", produces = "application/json")
	@ResponseBody
	public Long getApiHits() throws Exception {
		return this.restService.getApiHits();
	}

	/**
	 * @return Bookmarks count
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/bookmarks/count", produces = "application/json")
	@ResponseBody
	public Long getBookmarkCount() throws Exception {
		return this.dictionaryService.getBookmarksCount();
	}

	/**
	 * @return Bookmarks count
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/bookmarks/clear", produces = "application/json")
	@ResponseBody
	public boolean clearBookmarks() throws Exception {
		return this.dictionaryService.clearBookmarks();
	}

	

	/**
	 * @return Get Bookmarks in Pdf format
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/bookmarks/download", produces = "application/pdf")
	@ResponseBody
	public ResponseEntity<InputStreamResource> downloadBookmarks() throws Exception {
		HttpHeaders headers = new HttpHeaders();
		/* Prevent Caching of the document */
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
		/* Convert bytes to Inputstream which is to be passed in response body */
		byte[] pdfBytes = this.dictionaryService.getBookmarksPdf();
		ByteArrayInputStream stream = new ByteArrayInputStream(pdfBytes);
		headers.setContentDispositionFormData("inline", "Bookmarks.pdf");
		return ResponseEntity.ok().headers(headers).contentType(MediaType.parseMediaType("application/octet-stream"))
				.body(new InputStreamResource(stream));

	}
}
