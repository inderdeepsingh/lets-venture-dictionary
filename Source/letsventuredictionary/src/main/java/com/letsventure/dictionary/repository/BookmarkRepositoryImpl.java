package com.letsventure.dictionary.repository;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.letsventure.dictionary.entity.Bookmark;

/**
 * @author Inderdeep Singh Bookmark Repository Implementation Class
 */
@Repository
@Transactional
public class BookmarkRepositoryImpl implements BookmarkRepository {

	@Autowired
	private SessionFactory sessionFactory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.letsventure.dictionary.repository.BookmarkRepository#getBookmarks()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Bookmark> getBookmarks() {
		// TODO Auto-generated method stub

		Query queryObj = this.sessionFactory.getCurrentSession().createQuery("from Bookmark ");

		return queryObj.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.letsventure.dictionary.repository.BookmarkRepository#saveBookmark
	 * (com.letsventure.dictionary.entity.Bookmark)
	 */
	@Override
	public Bookmark saveBookmark(Bookmark bookmark) {
		// TODO Auto-generated method stub
		Serializable id = this.sessionFactory.getCurrentSession().save(bookmark);
		if (id != null) {
			bookmark.setBookmarkId((Long) id);
		} else {
			bookmark = null;
		}
		return bookmark;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.letsventure.dictionary.repository.BookmarkRepository#getBookmarksCount
	 * ()
	 */
	@Override
	public Long getBookmarksCount() {
		// TODO Auto-generated method stub
		Query queryObj = this.sessionFactory.getCurrentSession().createQuery("select count(1) from Bookmark ");

		return (Long) queryObj.list().get(0);
	}

	@Override
	public boolean clearBookmarks() {
		// TODO Auto-generated method stub
		Query queryObj = this.sessionFactory.getCurrentSession().createQuery("delete from Bookmark ");
		queryObj.executeUpdate();
		return true;
	}

	@Override
	public boolean bookmarkExists(String word) {
		// TODO Auto-generated method stub
		Query queryObj = this.sessionFactory.getCurrentSession().createQuery("from Bookmark where wordInfo.word=:word");
		queryObj.setParameter("word", word);
		return (queryObj.list() != null && (queryObj.list().size() > 0)) ? true : false;
	}
}
