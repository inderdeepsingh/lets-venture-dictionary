package com.letsventure.dictionary.service;

/**
 * @author Inderdeep Singh Service to interact with External Rest API
 *
 */
public interface RestService {
	/**
	 * Hit external Rest API to retrieve Data
	 * 
	 * @param query
	 *            parameter to be passed to API
	 * @param Expected
	 *            Response Object Class
	 * @return Response Object
	 * @throws Exception
	 */
	public Object makeRequestToApi(String query, Class<?> responseClass) throws Exception;

	/**
	 * @return count of API Hits from external Rest API
	 * @throws Exception
	 */
	public Long getApiHits() throws Exception;
}
