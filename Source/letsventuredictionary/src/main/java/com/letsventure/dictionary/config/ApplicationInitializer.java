package com.letsventure.dictionary.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.web.client.RestTemplate;

import com.letsventure.dictionary.messageconverter.MyGsonHttpMessageConverter;

/**
 * @author Inderdeep Singh This class is the entry point of application. It
 *         configures the required Datasource ,SessionFactory beans and Embedded
 *         Tomcat
 */
@EnableAutoConfiguration
@ComponentScan(basePackages = { ApplicationConstants.PACKAGES_TO_SCAN })
public class ApplicationInitializer extends SpringBootServletInitializer {

	// Data source object to be used for constructing session factory
	@Autowired
	private DataSource dataSource;

	/*
	 * @return Hibernate Session Factory Bean
	 */
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
		sessionFactoryBean.setDataSource(dataSource);
		sessionFactoryBean.setPackagesToScan(ApplicationConstants.ENTITY_PACKAGES_TO_SCAN);
		return sessionFactoryBean;
	};

	/*
	 * @return EmbeddedServletContainerFactory
	 */
	@Bean
	public EmbeddedServletContainerFactory servletContainer() {
		TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
		return factory;
	}

	/*
	 * @return RestTemplate
	 */
	@Bean
	public RestTemplate getRestTemplate() {
		RestTemplate template = new RestTemplate();
		template.getMessageConverters().add(new MyGsonHttpMessageConverter());
		return template;
	}

	/*
	 * @return SpringApplicationBuilder
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ApplicationInitializer.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(ApplicationInitializer.class, args);
	}
}
