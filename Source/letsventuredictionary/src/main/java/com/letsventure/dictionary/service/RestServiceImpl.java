package com.letsventure.dictionary.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

@Service
public class RestServiceImpl implements RestService {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${api.url}")
	private String apiUrl;

	/* (non-Javadoc)
	 * @see com.letsventure.dictionary.service.RestService#makeRequestToApi(java.lang.String, java.lang.Class)
	 */
	@Override
	public Object makeRequestToApi(String query, Class<?> responseClass) throws Exception {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		/*
		 * To hit a cloudflare Rest API from outside browser you will need to
		 * pass Useragent in headers otherwise it throws 403 Forbidden error
		 */
		headers.add(
				"User-Agent",
				"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		Map<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("type", "json");
		parametersMap.put("query", query);

		ResponseEntity<?> result = this.restTemplate.exchange(
				"https://letsventure.0x10.info/api/dictionary.php?type={type}&query={query}", HttpMethod.GET, entity,
				responseClass, parametersMap);
		if (result != null) {
			return result.getBody();
		} else {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.letsventure.dictionary.service.RestService#getApiHits()
	 */
	@Override
	public Long getApiHits() throws Exception {
		// TODO Auto-generated method stub
		String json = (String) makeRequestToApi("api_hits", String.class);
		Gson gson = new Gson();
		Properties properties = gson.fromJson(json, Properties.class);
		String apiHits = properties.getProperty("api_hits");
		if (apiHits != null && !apiHits.isEmpty()) {
			return Long.valueOf(apiHits);
		} else {
			return new Long(0);
		}

	}

}
