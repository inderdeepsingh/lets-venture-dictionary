package com.letsventure.dictionary.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.internal.LinkedTreeMap;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.letsventure.dictionary.entity.Bookmark;
import com.letsventure.dictionary.entity.WordInfo;
import com.letsventure.dictionary.repository.BookmarkRepository;

/**
 * @author Inderdeep Singh - Implementation Class for Dictionary Service
 *
 */
@Service
public class DictionaryServiceImpl implements DictionaryService {

	@Autowired
	private RestService restService;

	@Autowired
	private BookmarkRepository bookmarkRepository;

	/* (non-Javadoc)
	 * @see com.letsventure.dictionary.service.DictionaryService#getWords(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<WordInfo> getWords(String startingWith) throws Exception {
		List<WordInfo> wordList=new ArrayList<WordInfo>();
		WordInfo info=null;
		Object result = restService.makeRequestToApi(startingWith, List.class);
		if (result != null) {
			@SuppressWarnings("rawtypes")
			List<LinkedTreeMap> words=(List<LinkedTreeMap>) result;
			for(@SuppressWarnings("rawtypes") LinkedTreeMap word: words){
				info=new WordInfo();
				info.setWord(word.get("word")!=null?(String) word.get("word"):"");
				info.setDescription( word.get("description")!=null?(String) word.get("description"):"");
				info.setAudio_url(word.get("audio_url")!=null?(String) word.get("audio_url"):"");
				info.setBookmarked(this.bookmarkExists(info.getWord()));
				wordList.add(info);
			}
			
		} 
		return wordList;
	}

	/* (non-Javadoc)
	 * @see com.letsventure.dictionary.service.DictionaryService#saveBookmark(com.letsventure.dictionary.entity.Bookmark)
	 */
	@Override
	public Bookmark saveBookmark(Bookmark bookmark) throws Exception {
		// TODO Auto-generated method stub
		return this.bookmarkRepository.saveBookmark(bookmark);
	}

	/* (non-Javadoc)
	 * @see com.letsventure.dictionary.service.DictionaryService#getBookmarks()
	 */
	@Override
	public List<Bookmark> getBookmarks() throws Exception {
		// TODO Auto-generated method stub
		return this.bookmarkRepository.getBookmarks();
	}

	/* (non-Javadoc)
	 * @see com.letsventure.dictionary.service.DictionaryService#getBookmarksCount()
	 */
	@Override
	public Long getBookmarksCount() throws Exception {
		// TODO Auto-generated method stub
		return this.bookmarkRepository.getBookmarksCount();
	}

	/* (non-Javadoc)
	 * @see com.letsventure.dictionary.service.DictionaryService#clearBookmarks()
	 */
	@Override
	public boolean clearBookmarks() throws Exception {
		// TODO Auto-generated method stub
		return this.bookmarkRepository.clearBookmarks();
	}

	/* (non-Javadoc)
	 * @see com.letsventure.dictionary.service.DictionaryService#getBookmarksPdf()
	 */
	@Override
	public byte[] getBookmarksPdf() throws Exception {
		// TODO Auto-generated method stub
		//Get Bookmarks
		List<Bookmark> bookmarks = getBookmarks();
		//Create Document
		Document document = new Document();
		/*Write Data to ByteArrayOutputStream so that It can be converted to bytes*/
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		PdfWriter.getInstance(document, outputStream);
		document.open();
		Paragraph para =null;

		if (bookmarks.size() > 0) {			
			para = new Paragraph(new Chunk("Bookmarks", FontFactory.getFont(FontFactory.HELVETICA, 30)));
			PdfPTable table = new PdfPTable(2);
			//Write Bookmarks Table Header
			table.addCell("Word");
			table.addCell("Description");
			
			//Write Bookmarks
			for(Bookmark b :bookmarks){
				table.addCell(b.getWordInfo().getWord());
				table.addCell(b.getWordInfo().getDescription());
			}
			document.add(para);
			
			para = new Paragraph(new Chunk("\n", FontFactory.getFont(FontFactory.HELVETICA, 30)));
			document.add(para);
			
			document.add(table);
			
		}
		else {
			para = new Paragraph(new Chunk("No Bookmarks found", FontFactory.getFont(FontFactory.HELVETICA, 30)));
			document.add(para);
		}
		
		document.close();
		byte[] pdfBytes = outputStream.toByteArray();
		return pdfBytes;
	}

	@Override
	public boolean bookmarkExists(String word) {
		// TODO Auto-generated method stub
		return this.bookmarkRepository.bookmarkExists(word);
	}

}
