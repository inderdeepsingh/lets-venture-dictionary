package com.letsventure.dictionary.entity;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Inderdeep Singh JPA Entity class to represent a Bookmark
 *
 */
@Entity
@Table(name = "BOOKMARKS")
public class Bookmark {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "BOOKMARK_ID")
	private Long bookmarkId;

	@Embedded
	private WordInfo wordInfo;

	public Long getBookmarkId() {
		return bookmarkId;
	}

	public void setBookmarkId(Long bookmarkId) {
		this.bookmarkId = bookmarkId;
	}

	public WordInfo getWordInfo() {
		return wordInfo;
	}

	public void setWordInfo(WordInfo wordInfo) {
		this.wordInfo = wordInfo;
	}

}
