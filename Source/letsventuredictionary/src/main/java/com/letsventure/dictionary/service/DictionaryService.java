package com.letsventure.dictionary.service;

import java.util.List;

import com.letsventure.dictionary.entity.Bookmark;
import com.letsventure.dictionary.entity.WordInfo;

/**
 * @author Inderdeep Singh Dictionary Service interface- Service to interact
 *         with repository layer
 */
public interface DictionaryService {

	/**
	 * Get Words from the external Rest API starting with input
	 * 
	 * @param startingWith
	 * @return
	 * @throws Exception
	 */
	public List<WordInfo> getWords(String startingWith) throws Exception;

	/**
	 * Save a bookmark to server
	 * 
	 * @param bookmark
	 * @return Saved Bookmark
	 * @throws Exception
	 */
	public Bookmark saveBookmark(Bookmark bookmark) throws Exception;

	/**
	 * @return Bookmark List
	 * @throws Exception
	 */
	public List<Bookmark> getBookmarks() throws Exception;

	/**
	 * @return Bookmarks count
	 * @throws Exception
	 */
	public Long getBookmarksCount() throws Exception;

	/**
	 * Clear Bookmarks
	 * 
	 * @return
	 */
	public boolean clearBookmarks() throws Exception;
	
	/**
	 * @return bytes array of PDF Document
	 * @throws Exception
	 */
	public byte[] getBookmarksPdf() throws Exception;
	/**
	 * @return true if a bookmark for a word exists
	 */
	public boolean bookmarkExists(String word);

}
