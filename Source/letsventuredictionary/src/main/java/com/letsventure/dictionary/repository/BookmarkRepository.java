package com.letsventure.dictionary.repository;

import java.util.List;

import com.letsventure.dictionary.entity.Bookmark;

/**
 * @author Inderdeep Singh
 * Bookmark Repository Interface
 */
public interface BookmarkRepository{

	
	/**
	 * @return List of bookmarks
	 */
	public List<Bookmark> getBookmarks();
	
	/**
	 * Save a bookmark to database
	 * @param store
	 * @return
	 */
	public Bookmark saveBookmark(Bookmark bookmark);
	
	/**
	 * Get Bookmarks count
	 * @return
	 */
	public Long getBookmarksCount();
	
	/**
	 * Clear Bookmarks 
	 * @return
	 */
	public boolean clearBookmarks();
	
	
	/**
	 * @return true if a bookmark for a word exists
	 */
	public boolean bookmarkExists(String word);

}
