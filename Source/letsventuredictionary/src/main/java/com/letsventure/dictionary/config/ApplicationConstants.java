package com.letsventure.dictionary.config;

/**
 * @author Inderdeep Singh This interface holds constant values to be used
 *         across application
 */
public interface ApplicationConstants {
	// Packages to be scanned by spring for Spring managed beans
	public static final String PACKAGES_TO_SCAN = "com.letsventure.dictionary";
	// Packages to be scanned for JPA Entity beans
	public static final String ENTITY_PACKAGES_TO_SCAN = "com.letsventure.dictionary.entity";
}
